# Change Log

All notable changes to the "file-tag" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [1.0.0]

- Initial release

## [1.0.1]

### Fixed

- Tags were not loaded on start of editor due to faulty activationEvent id.

## [1.1.2]

### Fixed

- Noticed a discrepancy in versioning. This has been corrected and will be correct going forward.
