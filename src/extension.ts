import * as vscode from "vscode";

import TagDataProvider from "./data-providers/TagDataProvider";
import RegisterCommands from "./RegisterCommands";
import RegisterDataProviders from "./RegisterDataProviders";

export function activate(context: vscode.ExtensionContext) {
  const tagDataProvider = new TagDataProvider(context);

  const registerCommands = new RegisterCommands(context, tagDataProvider);
  const registerDataProviders = new RegisterDataProviders(tagDataProvider);

  [
    ...registerCommands.registerAll(),
    ...registerDataProviders.registerAll()
  ].forEach(sub => context.subscriptions.push(sub));
}

export function deactivate() {}
