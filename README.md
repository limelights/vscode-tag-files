# vscode-tag-file

This extension exists purely because checking in `TODOs` can sometimes be daunting or relying on the `TODO` author to clearly express the intention behind the `TODO`.
This is also a way of tagging any file with something like "Easy linting fixes" or "Uses legacy logic" and group them by that same tag and being able to have a clear overview on what files have which tag as a way of prioritizing them for yourself.

## Features

- A Tree View listing all tags and all files tagged with that tag.
- Select or create tags from Quick Picker or right-click menu.

## Extension Settings

None at the moment because there doesn't seem to be a use case for me yet.

## Known Issues

None at the moment.
